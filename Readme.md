Telegram WeatherBot

With this TelegramBot you can get current weather info and forecasts for Turku.
The bot uses OpenWeatherMap API and is programmed using python.

The bot recognizes following commands:
By sending the bot a message "/saa" you can get the current weather.
By sending the bot a message "/ennuste" you can get a forecast for the next 12 hours.
By sending the bot a message "/start" or "/help" you will get instructions.

Features still work in progress:
A function that will automatically send next day's weather forecast
every night at 6pm to a specified chat-id.

~~The bot is deployed to Heroku.
In Telegram the bot goes by name @AdBeCaBot.~~ (Due to changes in Heroku's services this bot is no longer available in Heroku.)

