import requests

url = 'http://api.openweathermap.org/data/2.5/forecast?q=Turku&units=metric&lang=fi&mode=XML&APPID=ff64c247a136f706923d1ee0d55d71e2'


response = requests.get(url)

if response:
    data = response.json()
    print(data["city"]["name"])
    for forecast in data['list'][0:5]:
        time = forecast["dt_txt"]
      #  city = forecast["name"]
        temp = forecast["main"]["temp"]
        humidity = forecast["main"]["humidity"]
        description = forecast["weather"][0]["description"]
        print(time, "Lämpötila: ", temp, " C, ", description,", Ilmankosteus: ", humidity,"%")
else:
    print("Something went wrong!")


