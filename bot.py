

# Telegram Bot for sending weather info.

# Location is now set to Turku. 
# By sending the bot a message "/saa" you can get the current weather.
# By sending the bot a message "/ennuste" you can get a forecast for the next 12 hours.
# By sending the bot a message "/start" or "/help" you will get instructions.

# Features still work in progress:
# A function that will automatically send next day's weather forecast every night at 6pm to a specified chat-id.

# The bot is started with "pipenv run python bot.py" and shut down with Ctrl-C.
# The bot's heroku address is https://weatherlady.herokuapp.com/.
# In Telegram the bot goes by name @AdBeCaBot.


import logging
import os
import requests
import schedule
import telegram.ext
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from dotenv import load_dotenv



load_dotenv()

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

PORT = int(os.environ.get('PORT', '8443'))


def start(update, context):
    #Send a message when the command /start is issued.
    update.message.reply_text(
        'Tervetuloa sääbottiin!\n Valitse seuraavista komennoista:\n' 
        '/saa = ajantasaiset säätiedot\n'
        '/ennuste = sääennuste seuraaville 12 tunnille')


def help(update, context):
    #Send a message when the command /help is issued.
    update.message.reply_text('Valitse seuraavista komennoista:\n' 
        '/saa = ajantasaiset säätiedot\n'
        '/ennuste = sääennuste seuraaville 12 tunnille')


def error(update, context):
    #Log Errors caused by Updates.
    logger.warning('Update "%s" caused error "%s"', update, context.error)


def saa(update, context):
    #Makes an API-request to get the current weather. If response status is 200, parses the data and send a message with weather info.

    url = f"http://api.openweathermap.org/data/2.5/weather?q=Turku&units=metric&lang=fi&mode=XML&APPID={os.environ['OPEN_WEATHER_ID']}"
    response = requests.get(url)

    if response:
        data = response.json()
        city = data["name"]
        temp = data["main"]["temp"]
        feel = data["main"]["feels_like"]
        humidity = data["main"]["humidity"]
        description = data["weather"][0]["description"]
        wind = data["wind"]["speed"]
        update.message.reply_text(
            f'{city}\n'
            f'{description}\n'
            f'Lämpötila: {temp} C\n'
            f'Tuntuu: {feel} C\n'
            f'Ilmankosteus: {humidity}%\n'
            f'Tuulennopeus: {wind} m/s')
       
    else:
        update.message.reply_text("Something went wrong")


def ennuste(update, context):
    #Makes an API-request to get the weather forecast. If response status is 200, parses the data and sends a message with weather forecast for the next 5 three-hour-intervalls.
   
    url = f"http://api.openweathermap.org/data/2.5/forecast?q=Turku&units=metric&lang=fi&mode=XML&APPID={os.environ['OPEN_WEATHER_ID']}"
    response = requests.get(url)
    message = ""

    if response:
        data = response.json()
        message += (data["city"]["name"]) + '\n'
   
        for forecast in data['list'][0:5]:
            time = forecast["dt_txt"]
            temp = forecast["main"]["temp"]
            feel = forecast["main"]["feels_like"]
            humidity = forecast["main"]["humidity"]
            description = forecast["weather"][0]["description"]
            wind = forecast["wind"]["speed"]
            message += (
                f'{time}\n'
                f'{description}\n'
                f'Lämpötila: {temp}C\n' 
                f'Tuntuu: {feel}C\n'
                f'Ilmankosteus: {humidity}%\n'
                f'Tuulennopeus: {wind} m/s\n\n')
        update.message.reply_text(message)        
    else:
        update.message.reply_text("Something went wrong!")


def callback(context: telegram.ext.CallbackContext):
    # Makes an API request to get the weather forecast. If response status is 200, parses the data 
    # and passes it to job-queue which sends a message automatically every 60 seconds to a specified chat-id.
    url = f"http://api.openweathermap.org/data/2.5/forecast?q=Turku&units=metric&lang=fi&mode=XML&APPID={os.environ['OPEN_WEATHER_ID']}"
    response = requests.get(url)
    message = ""

    if response:
        data = response.json()
        message += (data["city"]["name"]) + '\n'
   
        for forecast in data['list'][0:5]:
            time = forecast["dt_txt"]
            temp = forecast["main"]["temp"]
            feel = forecast["main"]["feels_like"]
            humidity = forecast["main"]["humidity"]
            description = forecast["weather"][0]["description"]
            wind = forecast["wind"]["speed"]
            message += (
                f'{time}\n'
                f'{description}\n'
                f'Lämpötila: {temp}C\n' 
                f'Tuntuu: {feel}C\n'
                f'Ilmankosteus: {humidity}%\n'
                f'Tuulennopeus: {wind} m/s\n\n') 
        context.bot.send_message(chat_id=os.getenv('TELEGRAM_CHAT_ID'), text=message)        
    else:
        update.message.reply_text("Something went wrong!")



def main():
    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    updater = Updater(os.getenv('TELEGRAM_TOKEN'), use_context=True)
    
    # j = updater.job_queue

    # Sends a weather forecast automatically every 60 seconds
    # job_minute = j.run_repeating(callback, interval=60, first=0)
    
    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))
    dp.add_handler(CommandHandler("saa", saa))
    dp.add_handler(CommandHandler("ennuste", ennuste))


    # log all errors
    dp.add_error_handler(error)


    # Use this when starting the bot in localhost
   # updater.start_polling()

    
    # Use this when starting the bot through Heroku
    updater.start_webhook(listen="0.0.0.0",
                          port=PORT,
                          url_path=os.getenv('TELEGRAM_TOKEN'))
    # updater.bot.set_webhook(url=settings.WEBHOOK_URL)
    updater.bot.set_webhook("https://weatherlady.herokuapp.com/" + os.getenv('TELEGRAM_TOKEN'))                   
   

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. 
    updater.idle()


if __name__ == '__main__':
    main()